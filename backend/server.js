const express= require('express')

const app = express();

const cors = require("cors");
const routerEmp = require("./routes/emp")
app.use(cors("*"));
app.use(express.json());
app.use("/emp", routerEmp);

app.listen(4000, "0.0.0.0", () => {
    console.log("server started on the port 4000")
});