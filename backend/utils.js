function createResult(error,data) {
    const result = {};
    if (error) {
        result['status'] = "error";

        result["error"] = error;
    } else {
        result["status"] = "success";
        result["data"] = data;
    }

    return result;
}

const mysql = require("mysql2");

const openConnection = () => {
    const connection = mysql.createConnection({
        uri: "mysql://db:3306",
        user: "root",
        password: "root",
        database: "empdb"
    });

    connection.connect();

    return connection;
};

module.exports = {
    createResult,
    openConnection
};